const std = @import("std");
const Allocator = std.mem.Allocator;

const util = @import("util.zig");

fn getRanges(line: []const u8, lo0: *usize, hi0: *usize, lo1: *usize, hi1: *usize) !void {
        var ranges_iter = std.mem.splitAny(u8, line, ",");
        var limits_iter = std.mem.splitAny(u8, ranges_iter.next().?, "-");
        lo0.* = try util.parseInt(limits_iter.next().?);
        hi0.* = try util.parseInt(limits_iter.next().?);
        limits_iter = std.mem.splitAny(u8, ranges_iter.next().?, "-");
        lo1.* = try util.parseInt(limits_iter.next().?);
        hi1.* = try util.parseInt(limits_iter.next().?);
}

fn problem4a(lines: util.StringList) !usize {
    var sum: usize = 0;
    for (lines.items) |line| {
        if (util.strEmpty(line)) continue;
        var lo0: usize = undefined;
        var hi0: usize = undefined;
        var lo1: usize = undefined;
        var hi1: usize = undefined;
        try getRanges(line, &lo0, &hi0, &lo1, &hi1);
        if ((lo1 <= lo0 and hi0 <= hi1) or (lo1 >= lo0 and hi0 >= hi1)) sum += 1;
    }
    return sum;
}

fn problem4b(lines: util.StringList) !usize {
    var sum: usize = 0;
    for (lines.items) |line| {
        if (util.strEmpty(line)) continue;
        var lo0: usize = undefined;
        var hi0: usize = undefined;
        var lo1: usize = undefined;
        var hi1: usize = undefined;
        try getRanges(line, &lo0, &hi0, &lo1, &hi1);
        const partial_overlap = (lo0 <= lo1 and lo1 <= hi0) or (lo0 <= hi1 and hi1 <= hi0);
        const total_overlap = (lo1 <= lo0 and hi0 <= hi1) or (lo1 >= lo0 and hi0 >= hi1);
        if (partial_overlap or total_overlap) sum += 1;
    }
    return sum;
}

pub fn problem4(allocator: *Allocator) !void {
    std.debug.print("Problem 4:\n", .{});
    const lines = try util.readLinesFromFile(allocator, "inputs/4.txt");
    defer {
        for (lines.items) |line| allocator.free(line);
        defer lines.deinit();
    }
    std.debug.print("\tA: {}\n", .{try problem4a(lines)});
    std.debug.print("\tB: {}\n", .{try problem4b(lines)});
}
