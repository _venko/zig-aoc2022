const std = @import("std");
const Allocator = std.mem.Allocator;

const util = @import("util.zig");
const problem1 = @import("p1.zig").problem1;
const problem2 = @import("p2.zig").problem2;
const problem3 = @import("p3.zig").problem3;
const problem4 = @import("p4.zig").problem4;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var allocator = gpa.allocator();

    try problem1(&allocator);
    try problem2(&allocator);
    try problem3(&allocator);
    try problem4(&allocator);
}
