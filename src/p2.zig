const std = @import("std");
const Allocator = std.mem.Allocator;

const util = @import("util.zig");

fn problem2a(lines: util.StringList) usize {
    const shape_scores = [_]usize{ 1, 2, 3 }; // Rock, Paper, Scissors
    const outcome_scores = [_][3]usize{
        [_]usize{ 3, 6, 0 }, // Opponent: Rock
        [_]usize{ 0, 3, 6 }, // Opponent: Paper
        [_]usize{ 6, 0, 3 }, // Opponent: Scissors
    };
    var points: usize = 0;
    for (lines.items) |round| {
        if (round.len < 3) continue;
        const opponent: u8 = round[0] - 'A';
        const you: u8 = round[2] - 'X';
        points += shape_scores[you] + outcome_scores[opponent][you];
    }
    return points;
}

fn problem2b(lines: util.StringList) usize {
    const shape_scores = [_]usize{ 1, 2, 3 }; // Rock, Paper, Scissors
    const outcome_scores = [_]usize{ 0, 3, 6 };
    const your_shape_index = [_][3]u8{
        [_]u8{ 2, 0, 1 }, // Opponent: Rock,
        [_]u8{ 0, 1, 2 }, // Opponent: Paper
        [_]u8{ 1, 2, 0 }, // Opponent: Scissors
    };
    var points: usize = 0;
    for (lines.items) |round| {
        if (round.len < 3) continue;
        const opponent: u8 = round[0] - 'A';
        const outcome: u8 = round[2] - 'X';
        const you: u8 = your_shape_index[opponent][outcome];
        points += shape_scores[you] + outcome_scores[outcome];
    }
    return points;
}

pub fn problem2(allocator: *Allocator) !void {
    std.debug.print("Problem 2:\n", .{});
    const lines = try util.readLinesFromFile(allocator, "inputs/2.txt");
    defer {
        for (lines.items) |line| allocator.free(line);
        defer lines.deinit();
    }
    std.debug.print("\tA: {}\n", .{problem2a(lines)});
    std.debug.print("\tB: {}\n", .{problem2b(lines)});
}
