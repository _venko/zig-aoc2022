const std = @import("std");
const Allocator = std.mem.Allocator;

const util = @import("util.zig");

fn problem3a(lines: util.StringList) usize {
    var sum: usize = 0;
    for (lines.items) |line| {
        var hits = std.mem.zeroes([128]u8);
        for (line[0 .. line.len / 2]) |elem| hits[elem] += 1;

        for (line[line.len / 2 ..]) |elem| {
            if (hits[elem] > 0) {
                if (std.ascii.isLower(elem)) {
                    const priority = 1 + (elem - 'a');
                    sum += priority;
                } else {
                    const priority = 27 + (elem - 'A');
                    sum += priority;
                }
                break;
            }
        }
    }
    return sum;
}

fn problem3b(lines: util.StringList) usize {
    var sum: usize = 0;
    var cursor: usize = 0;
    while (cursor < lines.items.len - 3) : (cursor += 3) {
        const badge: u8 = blk: {
            var bag0: u128 = 0;
            var bag1: u128 = 0;
            var bag2: u128 = 0;
            for (lines.items[cursor + 0]) |elem| bag0 |= @as(u128, 1) << @as(u7, @intCast(elem));
            for (lines.items[cursor + 1]) |elem| bag1 |= @as(u128, 1) << @as(u7, @intCast(elem));
            for (lines.items[cursor + 2]) |elem| bag2 |= @as(u128, 1) << @as(u7, @intCast(elem));
            const flattened: u128 = bag0 & bag1 & bag2;
            const matching_idx: ?usize = for (0..127) |i| {
                const mask = @as(u128, 1) << @as(u7, @intCast(i));
                if (flattened & mask != 0) break i;
            } else null;
            const b: u8 = @intCast(matching_idx.?);
            break :blk b;
        };
        const priority: usize =
            if (std.ascii.isLower(badge)) 1 + (badge - 'a') else 27 + (badge - 'A');
        sum += priority;
    }
    return sum;
}

pub fn problem3(allocator: *Allocator) !void {
    std.debug.print("Problem 3:\n", .{});
    const lines = try util.readLinesFromFile(allocator, "inputs/3.txt");
    defer {
        for (lines.items) |line| allocator.free(line);
        defer lines.deinit();
    }
    std.debug.print("\tA: {}\n", .{problem3a(lines)});
    std.debug.print("\tB: {}\n", .{problem3b(lines)});
}
