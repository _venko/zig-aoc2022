const std = @import("std");
const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;
pub const StringList = ArrayList([]u8);

const file_buffer_len = 1024 * 1000;

pub fn parseInt(str: []const u8) !usize {
    return std.fmt.parseInt(usize, str, 10);
}

pub fn strEmpty(str: []const u8) bool {
    return std.mem.eql(u8, str, "");
}

pub fn readLinesFromFile(allocator: *Allocator, path: []const u8) !StringList {
    const file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
    var lines = StringList.init(allocator.*);
    const file_buffer = try file.readToEndAlloc(allocator.*, file_buffer_len);
    defer allocator.free(file_buffer);
    var iter = std.mem.splitAny(u8, file_buffer, "\n");
    while (iter.peek() != null) {
        const line_slice = iter.next().?;
        const duped = try allocator.dupe(u8, line_slice);
        try lines.append(duped);
    }
    return lines;
}
