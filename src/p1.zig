const std = @import("std");
const Allocator = std.mem.Allocator;

const util = @import("util.zig");

fn problem1a(input: util.StringList) !usize {
    var biggest: usize = 0;
    var sum: usize = 0;
    for (input.items) |line| {
        if (util.strEmpty(line)) {
            biggest = @max(biggest, sum);
            sum = 0;
        } else {
            sum += try util.parseInt(line);
        }
    }
    return biggest;
}

fn problem1b(input: util.StringList) !usize {
    var biggest: [3]usize = [3]usize{ 0, 0, 0 };
    var sum: usize = 0;
    for (input.items) |line| {
        if (util.strEmpty(line)) {
            if (sum > biggest[0]) {
                biggest[2] = biggest[1];
                biggest[1] = biggest[0];
                biggest[0] = sum;
            } else if (sum > biggest[1]) {
                biggest[2] = biggest[1];
                biggest[1] = sum;
            } else if (sum > biggest[2]) {
                biggest[2] = sum;
            }
            sum = 0;
        } else {
            sum += try util.parseInt(line);
        }
    }
    return biggest[0] + biggest[1] + biggest[2];
}

pub fn problem1(allocator: *Allocator) !void {
    std.debug.print("Problem 1:\n", .{});
    const lines = try util.readLinesFromFile(allocator, "inputs/1.txt");
    defer {
        for (lines.items) |line| allocator.free(line);
        defer lines.deinit();
    }
    std.debug.print("\tA: {}\n", .{try problem1a(lines)});
    std.debug.print("\tB: {}\n", .{try problem1b(lines)});
}
